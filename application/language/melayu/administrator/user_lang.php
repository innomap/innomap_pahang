<?php

	$lang['delete'] = 'Padam';
	$lang['delete_user'] = 'Padam User';
	$lang['cancel'] = 'Batal';
	$lang['delete_confirm_message'] = 'Anda yakin akan memadam ';
	$lang['new_user'] = 'Tambah Pengguna';
	$lang['username'] = 'Kata Nama';
	$lang['name'] = 'Nama';
	$lang['role'] = 'Access Control Level';
	$lang['action'] = 'Tindakan';
	$lang['edit'] = 'Ubah';
	$lang['delete'] = 'Padam';
	$lang['district'] = 'Daerah';
	$lang['form_user'] = 'Form User';
	$lang['password'] = 'Kata Laluan';
	$lang['retype_password'] = 'Ulang Kata Laluan';
	$lang['ic_number'] = 'Nombor Kad Pengenalan';
	$lang['gender'] = 'Jantina';
	$lang['position'] = 'Jawatan';
	$lang['department'] = 'Nama Jabatan';
	$lang['address'] = 'Alamat';
	$lang['postcode'] = 'Poskod';
	$lang['select_district'] = 'Pilih Daerah';
	$lang['telp_no'] = 'No. Telefon (P)';
	$lang['telp_home_no'] = 'No. Telefon (H/P)';
	$lang['save'] = 'Simpan';
	$lang['change_password'] = 'Ubah Kata Laluan';
	$lang['state'] = 'Negeri';
        $lang['add_user'] = 'Tambah Pengguna';
        $lang['edit_user'] = 'Borang Pengguna';
        
        $lang['new_password'] = 'Kata Laluan Baru';
	$lang['retype_new_password'] = 'Ulang Kata Laluan Baru';
	$lang['email'] = 'Emel';
?>