<?php

	$lang['delete'] = 'Padam';
	$lang['delete_evaluator'] = 'Padam Penilai';
	$lang['cancel'] = 'Batal';
	$lang['delete_confirm_message'] = 'Anda yakin akan memadam ';
	$lang['new_evaluator'] = 'Tambah Penilai';
	$lang['name'] = 'Nama';
	$lang['description'] = 'Deskripsi';
	$lang['action'] = 'Tindakan';
	$lang['edit'] = 'Ubah';
	$lang['delete'] = 'Padam';
	$lang['form_evaluator'] = 'Borang Penilai';
	$lang['save'] = 'Simpan';
	$lang['content'] = 'Kandungan';
	$lang['group'] = 'Kumpulan';
	$lang['username'] = 'Kata Name';
	$lang['password'] = 'Kata Laluan';
	$lang['retype_password'] = 'Ulang Kata Laluan';
	$lang['select_group'] = 'Pilih Kumpulan';
	$lang['change_password'] = 'Ubah Kata Laluan';

?>