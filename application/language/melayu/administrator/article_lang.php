<?php

	$lang['delete'] = 'Padam';
	$lang['delete_article'] = 'Padam Artikel';
	$lang['cancel'] = 'Batal';
	$lang['delete_confirm_message'] = 'Anda yakin akan memadam ';
	$lang['new_article'] = 'Tambah Artikel';
	$lang['title'] = 'Judul';
	$lang['menu'] = 'Menu';
	$lang['action'] = 'Tindakan';
	$lang['edit'] = 'Ubah';
	$lang['delete'] = 'Padam';
	$lang['form_article'] = 'Borang Artikel';
	$lang['save'] = 'Simpan';
	$lang['content'] = 'Content';
?>