<?php

	$lang['delete'] = 'Padam';
	$lang['delete_group'] = 'Padam Group';
	$lang['cancel'] = 'Batal';
	$lang['delete_confirm_message'] = 'Anda yakin akan memadam ';
	$lang['new_group'] = 'Tambah Group';
	$lang['name'] = 'Nama';
	$lang['description'] = 'Deskripsi';
	$lang['action'] = 'Tindakan';
	$lang['edit'] = 'Ubah';
	$lang['delete'] = 'Padam';
	$lang['form_group'] = 'Form Group';
	$lang['save'] = 'Simpan';
	$lang['content'] = 'Content';
?>