<?php
	
	$lang['delete'] = 'Delete';
	$lang['delete_article'] = 'Delete Article';
	$lang['cancel'] = 'Cancel';
	$lang['delete_confirm_message'] = 'Are you sure want to delete ';
	$lang['new_article'] = 'New Article';
	$lang['title'] = 'Title';
	$lang['menu'] = 'Menu';
	$lang['action'] = 'Action';
	$lang['edit'] = 'Edit';
	$lang['delete'] = 'Delete';
	$lang['form_article'] = 'Form Article';
	$lang['save'] = 'Save';
	$lang['content'] = 'Content';
?>