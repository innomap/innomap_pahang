<?php
	
	$lang['delete'] = 'Delete';
	$lang['delete_evaluator'] = 'Delete Evaluator';
	$lang['cancel'] = 'Cancel';
	$lang['delete_confirm_message'] = 'Are you sure want to delete ';
	$lang['new_evaluator'] = 'New Evaluator';
	$lang['name'] = 'Name';
	$lang['description'] = 'Description';
	$lang['action'] = 'Action';
	$lang['edit'] = 'Edit';
	$lang['delete'] = 'Delete';
	$lang['form_evaluator'] = 'Form Evaluator';
	$lang['save'] = 'Save';
	$lang['content'] = 'Content';
	$lang['group'] = 'Group';
	$lang['username'] = 'Username';
	$lang['password'] = 'Password';
	$lang['retype_password'] = 'Retype Password';
	$lang['select_group'] = 'Select Group';
	$lang['change_password'] = 'Change Password';
?>