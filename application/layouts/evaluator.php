<?php
	$ol_user = $this->user_session->get_evaluator();
?>
<html>
	<head>
		<base href="<?= base_url() ?>" />
        <meta charset="UTF-8">
        <title>{{title}} | Innomap Pahang</title>
        <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
    <!--    <link rel="shortcut icon" href="<?= ASSETS_IMG ?>favicon.ico" type="image/x-icon">
        <link rel="icon" href="<?= ASSETS_IMG ?>favicon.ico" type="image/x-icon">-->
        <link href="<?= ASSETS_CSS ?>bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link rel="stylesheet" type="text/css" href="<?= ASSETS_CSS ?>bootstrapValidator.min.css" />
        <link rel="stylesheet" type="text/css" href="<?= ASSETS_CSS ?>font-awesome.min.css" />
        <link href="<?= ASSETS_CSS ?>datatables/dataTables.bootstrap.css" rel="stylesheet" type="text/css" />
        <link href="<?= ASSETS_CSS ?>site.css" rel="stylesheet" type="text/css" />
        {{styles}}
	</head>
	<body class="container-fluid <?= $ol_user ? 'bg-grey' : 'bg-turquoise-grad' ?>">
		<?php if($ol_user){ ?>
            <div class="header foot-menu margin-btm-20">
                <nav class="menu navbar navbar-default">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-header" aria-expanded="false" aria-controls="navbar">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar text-white"></span>
                            <span class="icon-bar text-white"></span>
                            <span class="icon-bar text-white"></span>
                        </button>
                        <a class="navbar-brand" href="<?= base_url() ?>">
                            <img alt="Innomap Pahang" src="<?= base_url().ASSETS_IMG.'logo-innomap-pahang.svg' ?>" height="100%">
                        </a>
                    </div>
                    <div id="navbar-header" class="collapse navbar-collapse">
                        <?php if($this->menu != "site" && $this->menu != "account"){ ?>
                            <ul class="nav navbar-nav">
                                <li class="<?= $this->menu == "application" ? "active" : "" ?>"><a href="<?= base_url().PATH_TO_EVALUATOR.'applications' ?>">Application</a></li>
                            </ul>
                        
                            <ul class="nav navbar-nav navbar-right logout-btn">
                                <li>
                                    <a href="<?= base_url().'accounts/logout' ?>"><span class="fa fa-sign-out"></span>Log Keluar</a>
                                </li>
                            </ul> 
                        <?php } ?>
                    </div>
                </nav>
            </div>
        <?php } ?>
		<div class="col-xs-12 col-sm-12 col-md-12 content">
	        {{content}}
	    </div>
        
        <div class="col-md-12 footer-default text-center">
            <div class="col-md-12 no-padding"><div class="span12"><hr/></div></div>
            <?php $this->load->view('partial/footer') ?>
        </div>
        
	    <script type="text/javascript">
	        var baseUrl = "<?=base_url()?>";
            var evaluatorUrl = "<?=base_url().PATH_TO_EVALUATOR?>";
	    </script>
	     <!-- jQuery -->
	    <script src="<?= ASSETS_JS ?>jquery-2.1.1.min.js"></script>
	    <!-- Bootstrap -->
	    <script src="<?= ASSETS_JS ?>bootstrap.min.js" type="text/javascript"></script>
	    <script src="<?= ASSETS_JS ?>bootstrapValidator.min.js" type="text/javascript"></script>
	    <!-- DATA TABES SCRIPT -->
	    <script src="<?= ASSETS_JS ?>plugins/datatables/jquery.dataTables.js" type="text/javascript"></script>
	    <script src="<?= ASSETS_JS ?>plugins/datatables/dataTables.bootstrap.js" type="text/javascript"></script>
	     <script src="<?= ASSETS_JS ?>bootbox.min.js" type="text/javascript"></script>
         <script src="<?= ASSETS_JS ?>common.js" type="text/javascript"></script>
	    {{scripts}}
	</body>
</html>