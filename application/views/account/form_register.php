<script type="text/javascript">
        var alert = "<?= $alert ?>";
</script>
<div class="col-md-12 col-sm-12 col-xs-12 container margin-top-50">
        <div class="col-md-7 col-sm-12 col-xs-12">
                <?php $this->load->view('partial/logo_front') ?>
        </div>
        <div class="col-md-5 col-sm-8 col-xs-12">
                <div class="col-md-12">
                        <div class="col-md-12 text-center">
                                <h2><?= lang('registration_form') ?></h2>
                        </div>

                        <div class="col-md-12">
                                <div class="alert alert-info alert-dismissable hide">
                                        <i class="fa fa-info"></i>
                                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                        <?= $alert ?>
                                </div>

                                <form id="form-register" class="form-horizontal margin-top-30" action="<?= base_url() . 'accounts/register_handler' ?>" method="POST" enctype="multipart/form-data">
                                        <div class="form-group">
                                                <label class="col-md-3 control-label"><?= lang('username') ?></label>
                                                <div class="col-md-9">
                                                        <!--<input type="text" class="form-control" name="username" placeholder="<? lang('username') ?>">-->
                                                        <input type="text" class="form-control" name="username" placeholder="Contoh: ahmad90 atau ahmad_90">
                                                </div>
                                        </div>

                                        <div class="form-group">
                                                <label class="col-md-3 control-label"><?= lang('password') ?></label>
                                                <div class="col-md-9">
                                                        <input type="password" class="form-control" name="password" placeholder="<?= lang('password') ?>">
                                                </div>
                                        </div>

                                        <div class="form-group">
                                                <label class="col-md-3 control-label"><?= lang('retype_password') ?></label>
                                                <div class="col-md-9">
                                                        <input type="password" class="form-control" name="retype_password" placeholder="<?= lang('retype_password') ?>">	
                                                </div>
                                        </div>

                                        <div class="form-group">
                                                <label class="col-md-3 control-label"><?= lang('application_type') ?></label>
                                                <div class="col-md-9">
                                                        <?php foreach ($application_types as $key => $value) { ?>
                                                                <div class="radio"> 
                                                                        <label> <input type="radio" name="application_type" value="<?= $key ?>" <?= $key == 0 ? 'checked' : '' ?>> <?= $value ?> </label> 
                                                                </div>
                                                        <?php } ?>
                                                </div>
                                        </div>

                                        <div class="form-group">
                                                <label class="col-md-3 control-label"><?= lang('name') ?></label>
                                                <div class="col-md-9">
                                                        <input type="text" class="form-control" name="name" placeholder="Nama seperti dalam MyKad">
                                                </div>
                                        </div>

                                        <div class="form-group">
                                                <label class="col-md-3 control-label"><?= lang('ic_number') ?></label>
                                                <div class="col-md-9">
                                                        <div class="input-group">
                                                                <input type="text" maxlength="6" class="form-control number-only" name="ic_num_1">
                                                                <span class="input-group-addon">-</span>
                                                                <input type="text" maxlength="2" class="form-control number-only" name="ic_num_2">
                                                                <span class="input-group-addon">-</span>
                                                                <input type="text" maxlength="4" class="form-control number-only" name="ic_num_3">
                                                        </div>
                                                </div>
                                        </div>

                                        <div class="form-group">
                                                <label class="col-md-3 control-label"><?= lang('gender') ?></label>
                                                <div class="col-md-9">
                                                        <?php foreach ($gender as $key => $value) { ?>
                                                                <div class="radio"> 
                                                                        <label> <input type="radio" name="gender" value="<?= $key ?>" <?= $key == 0 ? 'checked' : '' ?>> <?= $value ?> </label> 
                                                                </div>
                                                        <?php } ?>
                                                </div>
                                        </div>

                                        <div class="form-group">
                                                <label class="col-md-3 control-label"><?= lang('address') ?></label>
                                                <div class="col-md-9">
                                                        <textarea class="form-control" name="address" rows="5" placeholder="<?= lang('address') ?>"></textarea>
                                                        <input type="hidden" name="latitude">
                                                        <input type="hidden" name="longitude">
                                                </div>
                                        </div>

                                        <div class="form-group">
                                                <label class="col-md-3 control-label"><?= lang('postcode') ?></label>
                                                <div class="col-md-9">
                                                        <input type="text" class="form-control number-only" maxlength="5" name="postcode" placeholder="<?= lang('postcode') ?>">
                                                </div>
                                        </div>

                                        <div class="form-group">
                                                <label class="col-md-3 control-label"><?= lang('district') ?></label>
                                                <div class="col-md-9">
                                                        <select class="form-control" name="district_id">
                                                                <option value="">-- <?= lang('select_district') ?> --</option>
                                                                <?php foreach ($districts as $key => $value) { ?>
                                                                        <option value="<?= $value['id'] ?>"><?= $value['name'] ?></option>
                                                                <?php } ?>
                                                        </select>
                                                </div>
                                        </div>

                                        <div class="form-group">
                                                <label class="col-md-3 control-label"><?= lang('state') ?></label>
                                                <div class="col-md-9">
                                                        <input type="text" class="form-control" name="state" placeholder="<?= lang('state') ?>" value="Pahang" readonly>
                                                </div>
                                        </div>

                                        <div class="form-group">
                                                <label class="col-md-3 control-label"><?= lang('telp_no') ?></label>
                                                <div class="col-md-9">
                                                        <input type="text" class="form-control number-only" minlength="10" maxlength="11" name="telp_no" placeholder="<?= lang('placeholder_telp_no') ?>">
                                                </div>
                                        </div>

                                        <div class="form-group">
                                                <label class="col-md-3 control-label"><?= lang('telp_home_no') ?></label>
                                                <div class="col-md-9">
                                                        <input type="text" class="form-control number-only" minlength="9" maxlength="10" name="telp_home_no" placeholder="<?= lang('placeholder_telp_home_no') ?>">
                                                </div>
                                        </div>

                                        <div class="form-group">
                                                <label class="col-md-3 control-label"><?= lang('email') ?></label>
                                                <div class="col-md-9">
                                                        <input type="email" class="form-control" name="email" placeholder="Contoh: xyz@gmail.com">
                                                </div>
                                        </div>

                                        <div class="form-group file-field">
                                                <label class="col-md-3 control-label"><?= lang('photo') ?></label>
                                                <div class="col-md-9">
                                                        <input name="innovator_photo"
                                                               type="file" 
                                                               accept="image/x-png, image/x-PNG, image/jpeg, image/jpg, image/JPEG, image/JPG"
                                                               class="form-control"
                                                               >
                                                </div>
                                        </div>				

                                        <div class="form-group collapse">
                                                <label class="col-md-3 control-label"><?= lang('team_member') ?></label>
                                                <div class="col-md-9 team-member-wrap">
                                                        <div class="col-md-12 team-member-item no-padding">
                                                                <input type="hidden" class="team-member-id" name="team_member_id[]" value="0">
                                                                <div class="col-md-11 no-padding">
                                                                        <div class="col-md-6 no-padding-left">
                                                                                <input type="text"class="form-control" name="team_member_0" placeholder="<?= lang('name') ?>">
                                                                        </div>
                                                                        <div class="col-md-6 no-padding-left">
                                                                                <input type="text"class="form-control" name="team_member_ic_num_0" placeholder="<?= lang('ic_number') ?>">
                                                                        </div>
                                                                </div>
                                                                <div class="col-md-1 no-padding">
                                                                        <button type="button" class="btn btn-default add-team-member"><span class="fa fa-plus"></span></button>
                                                                </div>
                                                                <div class="col-md-12 no-padding-left padding-top-10">
                                                                        <div class="col-md-3">
                                                                                <label><?= lang('photo_team') ?></label>
                                                                        </div>
                                                                        <div class="col-md-9 no-padding">
                                                                                <input type="file" class="form-control" name="team_member_pic_0">
                                                                        </div>
                                                                </div>
                                                        </div>
                                                </div>
                                        </div>

                                        <div class="form-group text-center">
                                                <input type="submit" name="btn_register" class="btn btn-black" value="<?= lang('register') ?>">
                                        </div>
                                </form>

                                <div class="col-md-12 text-center">
                                        <span><?= lang('already_have_account') ?> <a href="<?= base_url() ?>" class="link-black">Masuk</a></span>
                                </div>
                        </div>
                </div>
        </div>
</div>