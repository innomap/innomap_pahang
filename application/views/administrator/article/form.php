<script type="text/javascript">
	var lang_delete = "<?= lang('delete') ?>",
		lang_delete_user = "<?= lang('delete_user') ?>",
		lang_cancel = "<?= lang('cancel') ?>",
		lang_delete_confirm_message = "<?= lang('delete_confirm_message') ?>",
		edit_mode = "<?= (isset($article) ? 1 : 0) ?>";
</script>
<?php $this->load->view('partial/logo') ?>
<div class="col-md-8 col-md-offset-2 content-box bg-light-grey">
	<div class="col-md-12 text-center">
		<h2><?= lang('form_article') ?></h2>
    </div>
	<form role="form" class="form-horizontal" id="form-article" action="<?= site_url(PATH_TO_ADMIN.'articles/'.$form_action.'/') ?>" method="POST" enctype="multipart/form-data">
		<input type="hidden" name="id" value="<?= isset($article) ? $article['id'] : '' ?>" />
			<div class="form-group">
				<label class="col-md-3 control-label"><?= lang('menu') ?></label>
				<div class="col-md-9">
					<input type="text" name="menu" class="form-control" placeholder="<?= lang('menu') ?>" value="<?= isset($article) ? $article['menu'] : '' ?>">
				</div>
			</div>

			<div class="form-group">
				<label class="col-md-3 control-label"><?= lang('title') ?></label>
				<div class="col-md-9">
					<input type="text" name="title" class="form-control" placeholder="<?= lang('title') ?>" value="<?= isset($article) ? $article['title'] : '' ?>">
				</div>
			</div>

			<div class="form-group">
				<label class="col-md-3 control-label"><?= lang('content') ?></label>
				<div class="col-md-9">
					<textarea name="content" rows="6" id="content-editor" class="form-control" placeholder="<?= lang('content') ?>"><?= isset($article) ? $article['content'] : '' ?></textarea>
				</div>
			</div>
			
			<div class="form-group text-center">
				<button type="button" class="btn btn-grey btn-cancel"><?= lang('cancel') ?></button>
				<button type="submit" class="btn btn-dark-turquoise flat"><?= lang('save') ?></button>
			</div>
		</form>
</div>