<div class="col-md-12 report_type" data-content="application-num-by-category">
	<div class="row">
		<form id="form-application-num-by-cat" action="<?= base_url().PATH_TO_ADMIN.'reports/export_application_number_by_category' ?>" method="post">
			<div class="form-group col-md-12">
				<div class="col-md-3">
					<button type="submit" class="btn btn-success flat" id="export-application-num">Export to Excel</button>
				</div>
			</div>
		</form>
	</div>

	<div class="col-md-12" id="container-application-num-by-cat">
	</div>
</div>

<?= $modal_detail; ?>