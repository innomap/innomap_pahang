<?php
	$ol_user = $this->user_session->get_user();
	$ol_evaluator = $this->user_session->get_evaluator();
	$ol_admin = $this->user_session->get_admin();
?>
<div class="col-md-12">
	<div class="col-md-2 col-md-offset-5">
		<div class="col-md-6 col-sm-6 col-xs-6">
			<a href="http://www.pahang.gov.my/" target="_blank"><img src="<?= base_url().ASSETS_IMG.'logo-state-pahang.svg' ?>" width="100%"/></a>
		</div>
		<div class="col-md-6 col-sm-6 col-xs-6">
			<a href="http://www.yim.my/yim_v3/index.html" target="_blank"><img src="<?= base_url().ASSETS_IMG.'logo-yim.svg' ?>" width="110%"/></a>
		</div>
	</div>
	<?php if(!$ol_user && !$ol_evaluator && !$ol_admin){ ?>
	<div class="col-md-12">
		<nav class="navbar navbar-basic text-center">
            <div>
                <ul class="nav navbar-nav navbar-center">
                    <?php foreach ($this->header_menu as $key => $value) { ?>
                        <li class="<?= $this->uri->segment(3) == $value['url'] ? 'active' : '' ?>"><a href="<?= base_url().'site/article/'.$value['url'] ?>"><?= $value['menu'] ?></a></li>
                    <?php } ?>
                </ul>
            </div>
        </nav>
	</div>
	<?php } ?>
	<div class="col-md-12 footer-text">
		Copyright &copy 2016 YIM Technology Resources Sdn Bhd. All Rights Reserved.
	</div>
</div>