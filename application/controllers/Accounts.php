<?php

defined('BASEPATH') OR exit('No direct script access allowed');

require_once(APPPATH . 'controllers/Common.php');

class Accounts extends Common {

        function __construct() {
                parent::__construct("account");

                $this->load->model('user');
                $this->load->model('innovator');
                $this->load->model('district');
                $this->load->model('evaluator');

                $this->lang->load('account', $this->language);

                $this->scripts[] = 'site/account';
        }

        public function index() {
                $this->user = $this->user_session->get_user();
                if ($this->user) {
                        redirect(base_url() . 'applications');
                } else {
                        $this->title = "Home";
                        $this->scripts[] = 'site/landing';
                        $data['alert'] = $this->session->flashdata('alert');
                        $this->load->view('site/index', $data);
                }
        }

        function register() {
                $this->title = "Register";
                $this->scripts[] = 'plugins/autocomplete/bootstrap3-typeahead.min';
                $this->styles[] = 'autocomplete/typeahead';

                $data['alert'] = $this->session->flashdata('alert');
                $data['application_types'] = unserialize(APPLICATION_TYPE);
                $data['gender'] = unserialize(GENDER);
                $data['districts'] = $this->district->find_all();
                $this->load->view('account/form_register', $data);
        }

        function register_handler() {
                $this->layout = FALSE;
                $status = 0;

                $postdata = $this->postdata();

                if ($postdata['username'] != "" && $postdata['name'] != "" && $postdata['district_id'] != "") {
                        if ($this->user->is_username_exist($postdata['username'])) {
                                $this->session->set_flashdata('alert', 'Sorry, your username has been registered.');
                        } else {
                                $data = array(
                                    "username" => $postdata['username'],
                                    "password" => $postdata['password'],
                                    "status" => USER_STATUS_ACTIVE,
                                    "role_id" => USER_ROLE_INNOVATOR,
                                    "created_at" => date('Y-m-d H:i:s'));

                                if ($id = $this->user->insert_user($data)) {
                                        $innovator = array("user_id" => $id,
                                            "name" => $postdata['name'],
                                            "application_type" => $postdata['application_type'],
                                            "ic_number" => $postdata['ic_num_1'] . "-" . $postdata['ic_num_2'] . "-" . $postdata['ic_num_3'],
                                            "gender" => $postdata['gender'],
                                            "address" => $postdata['address'],
                                            "postcode" => $postdata['postcode'],
                                            "district_id" => $postdata['district_id'],
                                            "telp_home_no" => $postdata['telp_home_no'],
                                            "telp_no" => $postdata['telp_no'],
                                            "email" => $postdata['email'],
                                            "latitude" => $postdata['latitude'],
                                            "longitude" => $postdata['longitude']);
                                        //save photo
                                        if (isset($_FILES['innovator_photo'])) {
                                                if ($_FILES['innovator_photo']['name'] != NULL) {
                                                        $photo_filename = $this->generate_filename($id, $_FILES['innovator_photo']['name']);
                                                        $uploaded_photo = $this->_upload($photo_filename, 'innovator_photo', PATH_TO_INNOVATOR_PHOTO, PATH_TO_INNOVATOR_PHOTO_THUMB);

                                                        $innovator['photo'] = $uploaded_photo;
                                                }
                                        }

                                        if ($this->innovator->insert($innovator)) {
                                                $this->save_team_member($id, $postdata);
                                                $status = 1;
                                                $this->session->set_flashdata('alert', 'Registration successful.');
                                                $this->auth($postdata['username'], $postdata['password']);
                                        }
                                } else {
                                        $this->session->set_flashdata('alert', 'Sorry, an error occurred, please try again later.');
                                }
                        }
                }

                if ($status == 0) {
                        redirect(base_url() . 'register');
                }
        }

        private function save_team_member($id, $postdata) {
                $this->load->model('innovator_expert_team');

                foreach ($postdata['team_member_id'] as $key => $value) {
                        if ($postdata['team_member_' . $key] != "") {
                                $data_member = array('user_id' => $id,
                                    'name' => $postdata['team_member_' . $key],
                                    'ic_number' => $postdata['team_member_ic_num_' . $key]);

                                if (isset($_FILES['team_member_pic_' . $key])) {
                                        if ($_FILES['team_member_pic_' . $key]['name'] != NULL) {
                                                $photo_filename = $this->generate_filename($id, $_FILES['team_member_pic_' . $key]['name']);
                                                $uploaded_photo = $this->_upload($photo_filename, 'team_member_pic_' . $key, PATH_TO_TEAM_MEMBER_PHOTO, PATH_TO_TEAM_MEMBER_PHOTO_THUMB);

                                                $data_member['photo'] = $uploaded_photo;
                                        }
                                }

                                $this->innovator_expert_team->insert($data_member);
                        }
                }
        }

        private function _upload($name, $attachment, $upload_path, $thumb_path = NULL) {
                $this->load->library('upload');
                $config['file_name'] = $name;
                $config['upload_path'] = $upload_path;
                $config['allowed_types'] = 'png|jpg|gif|bmp|jpeg';
                $config['remove_spaces'] = TRUE;

                $this->upload->initialize($config);
                if (!$this->upload->do_upload($attachment, true)) {
                        echo $this->upload->display_errors();
                        return false;
                } else {
                        $upload_data = $this->upload->data();
                        if ($thumb_path != NULL) {
                                $this->create_thumbnail($upload_data['image_width'], $upload_data['image_height'], $upload_path, $upload_data['file_name'], $thumb_path);
                        }
                        return $upload_data['file_name'];
                }
        }

        private function create_thumbnail($image_width, $image_height, $upload_path, $file_name, $new_path) {
                $this->load->library('image_lib');

                if ($image_width > 1024 || $image_height > 1024) {
                        $config['image_library'] = 'gd2';
                        $config['source_image'] = $upload_path . $file_name;
                        $config['new_image'] = $new_path . $file_name;
                        $config['maintain_ratio'] = TRUE;
                        $config['width'] = 1024;
                        $config['height'] = 1024;

                        $this->image_lib->clear();
                        $this->image_lib->initialize($config);

                        if (!$this->image_lib->resize()) {
                                echo $this->image_lib->display_errors();
                                return false;
                        }
                } else {
                        $config['image_library'] = 'gd2';
                        $config['source_image'] = $upload_path . $file_name;
                        $config['new_image'] = $new_path . $file_name;
                        $config['maintain_ratio'] = TRUE;
                        $config['width'] = $image_width;
                        $config['height'] = $image_height;

                        $this->image_lib->clear();
                        $this->image_lib->initialize($config);

                        if (!$this->image_lib->resize()) {
                                echo $this->image_lib->display_errors();
                                return false;
                        }
                }
        }

        private function generate_filename($id, $filename) {
                $attachment_name = preg_replace('/\s+/', '_', $filename);
                $attachment_name = str_replace('.', '_', $attachment_name);
                $retval = $id . "_" . $attachment_name;

                return $retval;
        }

        function check_username_exist() {
                $this->layout = FALSE;

                $username = $this->input->post('username');
                $id = (isset($_POST['id']) ? $this->input->post('id') : 0);
                if ($this->user->is_username_exist($username, $id)) {
                        $retval = false;
                } else {
                        $retval = true;
                }

                echo json_encode(array('valid' => $retval));
        }

        function check_ic_number_exist() {
                $this->layout = FALSE;

                $ic_num_1 = $this->input->post('ic_num_1');
                $ic_num_2 = $this->input->post('ic_num_2');
                $ic_num_3 = $this->input->post('ic_num_3');
                $ic_num = $ic_num_1 + '-' + $ic_num_2 + '-' + $ic_num_3;
                $id = isset($this->userdata) ? $this->userdata['id'] : 0;
                $this->load->model('innovator');
                if ($this->innovator->is_ic_number_exist($ic_num, $id)) {
                        $retval = false;
                } else {
                        $retval = true;
                }

                echo json_encode(array('valid' => $retval));
        }
        
        private function auth($username, $password) {
                $response = array();

                $user = $this->user->find_one('username = ' . $this->db->escape($username) . ' AND (role_id = ' . USER_ROLE_INNOVATOR . ' OR role_id = ' . USER_ROLE_EVALUATOR . ')');

                if ($user != NULL) {
                        if ($this->check_password($user['username'], $password, $user['password'])) {
                                $data_session = array('id' => $user['id'], 'username' => $user['username'], 'role_id' => $user['role_id']);

                                if ($user['role_id'] == USER_ROLE_INNOVATOR) {
                                        $innovator = $this->innovator->find_one("user_id = " . $user['id']);
                                        if ($innovator) {
                                                $this->user_session->set_user($data_session);
                                                redirect(base_url() . 'accounts');
                                        } else {
                                                $this->session->set_flashdata('alert', 'The username or password you entered is incorrect.');
                                                redirect(base_url());
                                        }
                                } else if ($user['role_id'] == USER_ROLE_EVALUATOR) {
                                        $evaluator = $this->evaluator->find_one("user_id = " . $user['id']);
                                        if ($evaluator) {
                                                $this->user_session->set_evaluator($data_session);
                                                redirect(base_url() . PATH_TO_EVALUATOR . 'applications');
                                        } else {
                                                $this->session->set_flashdata('alert', 'The username or password you entered is incorrect.');
                                                redirect(base_url());
                                        }
                                }
                        } else {
                                $this->session->set_flashdata('alert', 'The username or password you entered is incorrect.');
                                redirect(base_url());
                        }
                } else {
                        $this->session->set_flashdata('alert', 'The username or password you entered is incorrect.');
                        redirect(base_url());
                }
        }

        private function check_password($username, $password, $hash) {
                $password = $this->user->get_hash($username, $password);
                if ($password == $hash) {
                        return true;
                } else {
                        return false;
                }
        }

        public function login() {
                $this->title = "Login";
                $data['alert'] = $this->session->flashdata('alert');
                $this->load->view('account/form_login', $data);
        }

        function login_auth() {
                $this->layout = FALSE;

                $postdata = $this->postdata();
                $this->auth($postdata['username'], $postdata['password']);
        }

        public function logout() {
                $this->user_session->clear();
                redirect(base_url());
        }

        private function postdata() {
                if ($post = $this->input->post()) {
                        return $post;
                }
                redirect('accounts');
        }

}
