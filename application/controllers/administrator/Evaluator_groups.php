<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require_once(APPPATH . 'controllers/'.PATH_TO_ADMIN.'/Common.php');
class Evaluator_groups extends Common {

	function __construct() {
		parent::__construct();

		$this->title = "Manage Evaluator Group";
		$this->menu = "evaluator_group";

		$this->load->model('evaluator_group');

		$this->lang->load(PATH_TO_ADMIN.'evaluator_group',$this->language);

		$this->scripts[] = 'administrator/evaluator_group';
    }

    public function index(){
    	$data['alert'] = $this->session->flashdata('alert');
        $data['evaluator_groups'] = $this->evaluator_group->find_all();

        $this->load->view(PATH_TO_ADMIN.'evaluator_group/list', $data);
    }

    function add(){
        $data['form_action'] = 'save';

        $this->load->view(PATH_TO_ADMIN.'evaluator_group/form',$data);
    }

    function save(){
    	$this->layout = FALSE;

        $postdata = $this->postdata();

        if($postdata['name'] != ""){
            $data = array(
                "name" => $postdata['name'],
                "description" => $postdata['description']);

            if($postdata['id'] > 0){
                $id = $this->evaluator_group->update($postdata['id'],$data);
            }else{
                $id = $this->evaluator_group->insert($data);
            }
        
            if($postdata['id'] > 0){
                $this->session->set_flashdata('alert','Group has been updated.');
            }else{
                $this->session->set_flashdata('alert','Group has been added.');
            }
        }

        redirect(base_url().PATH_TO_ADMIN.'evaluator_groups');
    }

    public function edit($id = 0){
        $data['form_action'] = 'save';
        $data['group'] = $this->evaluator_group->find_one("id = ".$id);
		$this->load->view(PATH_TO_ADMIN.'evaluator_group/form',$data);
	}

    private function postdata(){
        if($post = $this->input->post()){
            return $post;
        }
        redirect(base_url().PATH_TO_ADMIN.'evaluator_groups');
    }

    function delete($id){
        $this->layout = FALSE;
        if($this->evaluator_group->delete($id)){
            $this->session->set_flashdata('alert','Group has been deleted.');
        }else{
            $this->session->set_flashdata('alert','Group can not be deleted.');
        }

        redirect(base_url().PATH_TO_ADMIN.'evaluator_groups');
    }
}
