<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Common extends CI_Controller {

        function __construct($module = NULL) {
                parent::__construct();

                $this->layout = DEFAULT_LAYOUT;

                $this->language = LANGUAGE_MELAYU;
                $this->submenu = "";

                $this->load->model('article');
                $this->menu = array();
                $this->header_menu = $this->article->find_all();

                if ($module != "account" && $module != "site") {
                        $this->userdata = $this->user_session->get_user();
                        if (!$this->userdata) {
                                redirect(base_url());
                        }
                }
        }

        function generate_pdf($id, $auto_download = TRUE) {
                $this->layout = FALSE;
                $this->load->helper(array('dompdf', 'file'));
                $this->load->model('application');
                $this->load->model('innovator');
                $this->load->model('application_picture');
                $this->load->model('district');

                $this->lang->load('application', $this->language);

                $data['innovator'] = $this->innovator->get_one_join($this->userdata['id']);
                if ($data['innovator']) {
                        $data['innovator']['district'] = $this->district->find_one("id = " . $data['innovator']['district_id']);
                        $data['innovator']['team_experts'] = $this->innovator_expert_team->find("user_id = " . $data['innovator']['user_id']);
                }

                $data['application_types'] = unserialize(APPLICATION_TYPE);
                $data['gender'] = unserialize(GENDER);
                $data['innovation_category'] = unserialize(INNOVATION_CATEGORY);
                $data['targets'] = unserialize(INNOVATION_TARGET);
                $data['troubleshootings'] = unserialize(TROUBLESHOOTING_OPTION);
                $data['estimated_market_size'] = unserialize(ESTIMATED_MARKET_SIZE);
                $data['income'] = unserialize(INCOME_OPTION);

                if ($data['application'] = $this->application->find_by_id($id)) {
                        $data['i_picture'] = $this->application_picture->find("application_id = " . $id);
                        $data['i_target'] = ($data['application']['target'] != NULL ? json_decode($data['application']['target']) : array());
                        $data['i_troubleshooting'] = ($data['application']['troubleshooting'] != NULL ? json_decode($data['application']['troubleshooting']) : array());
                }

                $filename = $id . "_" . str_replace(" ", "_", $data['application']['title']) . ".pdf";
                
                $this->application->update($id, array(
                    "pdf" => $filename,
                ));
                
                $html = $this->load->view('application/form_pdf', $data, true);
                
                generate_pdf($html, $filename, $auto_download);
                
        }

}
