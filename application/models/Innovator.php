<?php

require_once(APPPATH . 'models/General_model.php');
class Innovator extends General_model {
	function __construct() {
		parent::__construct();		
		$this->table_name = "innovator";
		$this->primary_field = "user_id";
	}

	function get_one_join($user_id){
		$this->db->select("innovator.*, user.username");
		$this->db->from("innovator");
		$this->db->join('user','user.id = innovator.user_id');
		$this->db->where('innovator.user_id', $user_id);
		$q = $this->db->get();

		return $q->row_array();
	}

	function get_join($where = NULL){
		$this->db->select("innovator.*, district.name as district_name, user.requested_to_delete, user.requested_to_delete_by");
		$this->db->from("innovator");
		$this->db->join("district","district.id = innovator.district_id");
		$this->db->join("user","user.id = innovator.user_id");
		if($where != NULL){
			$this->db->where($where);
		}
		$q = $this->db->get();

		return $q;
	}
        
        function get_registration_number_by_date($where = NULL, $limit = NULL, $order_by = NULL) {
                $this->db->select("innovator.*, user.username, user.status, user.created_at");
                $this->db->from("innovator");
                $this->db->join('user', 'user.id = innovator.user_id');
//                $this->db->join('state', 'innovator.state_id = state.id');
//                $this->db->join('country', 'country.id = state.country_id');
                $this->db->where('role_id', '0');
                $this->db->where('status', '1');
                if ($where != NULL) {
                        $this->db->where($where);
                }
                if ($limit != NULL) {
                        $this->db->limit($limit);
                }
                if ($order_by != NULL) {
                        $this->db->order_by($order_by);
                }
                return $this->db->get();
        }
        
        function is_ic_number_exist($ic_number, $user_id = 0){
		$user = $this->find_one('ic_number = "'.$ic_number.'"'.(!$user_id ? ' AND id != '.$user_id : ''));
		if($user){
			return true;
		}else{
			return false;
		}
	}
}

?>