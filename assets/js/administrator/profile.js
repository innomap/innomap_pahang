var formProfile = $('#form-profile');

$(function(){
	initAlert();
	initEditPassword();
	initValidator();

	initAutoGenerateGender();
});

function initAlert(){
	if(alert != ''){
		$('.alert-info').removeClass('hide').hide().fadeIn(500, function(){
			$(this).delay(3000).fadeOut(500);	
		})
	}
}

function initEditPassword(){
	formProfile.on('click','.btn-change-pass',function(e){
		e.preventDefault();
	    var elem = $(this);
	    var collapse = elem.parent().parent().parent().find('.collapse');
	    collapse.collapse('toggle');
	});
}

function initValidator(){
        $('.number-only').keypress(function (e) {
                var mInput = $(this).val() + "";
                console.log(mInput);
                if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
                        return false;
                }
        });
        
        var errorMsg = "Input tidak sah";
        
	formProfile.bootstrapValidator({
		feedbackIcons: {
			valid: 'glyphicon glyphicon-ok',
			invalid: 'glyphicon glyphicon-remove',
			validating: 'glyphicon glyphicon-refresh'
		},
		fields: {
                        password: {
                                validators: {
                                        notEmpty: {message: errorMsg}
                                }
                        },
                        retype_password: {
                                validators: {
                                        notEmpty: {message: errorMsg},
                                        identical: {
                                                field: 'password',
                                                message: 'Kata laluan tidak sepadan pengesahan'
                                        }
                                }
                        },
			name: {
				validators: {
					notEmpty: {message: errorMsg}
				}
			},
			ic_num_1: {
				validators: {
					notEmpty: {message: errorMsg},
					numeric: {message: errorMsg},
					stringLength: {
                        max: 6,
                        min:6,
                        message: 'Input mesti 6 aksara'
                    }
				}
			},
			ic_num_2: {
				validators: {
					notEmpty: {message: errorMsg},
					numeric: {message: 'The field must be a number'},
					stringLength: {
                        max: 2,
                        min:2,
                        message: 'Input mesti 2 aksara'
                    }
				}
			},
			ic_num_3: {
				validators: {
					notEmpty: {message: errorMsg},
					numeric: {message: 'Input mesti dalam bentuk nombor'},
					stringLength: {
                        max: 4,
                        min:4,
                        message: 'Input mesti 4 aksara'
                    }
				}
			},
			position:{
				validators: {
					notEmpty: {message: errorMsg},
				}
			},
			department:{
				validators: {
					notEmpty: {message: errorMsg},
				}
			},
			address:{
				validators: {
					notEmpty: {message: errorMsg},
				}
			},
			postcode:{
				validators: {
					notEmpty: {message: errorMsg},
				}
			},
			telp_no:{
				validators: {
					notEmpty: {message: errorMsg},
				}
			},
			email: {
                validators: {
                        emailAddress: {message: errorMsg},
                }
        	},
		}
	});
}

function initAutoGenerateGender(){
	formProfile.find('input[name=ic_num_3]').on('keyup', function(){
		var field_val = $(this).val(),
			ic_no_length = field_val.length;
		if(ic_no_length == 4){
			var last_two = parseInt(field_val.substr(ic_no_length-2));
			if(last_two%2 == 1){
				//odd
				$('input[name="gender"][value="0"]').prop('checked', true);
			}else{
				//even
				$('input[name="gender"][value="1"]').prop('checked', true);
			}
		}
	});
}
